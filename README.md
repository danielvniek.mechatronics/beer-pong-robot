# Description #
This project was done in a group with four other members as part of the fourth year Mechatronics subject at Stellenbosch University. The robot shoots ping pong balls into cups using pressurised air. The robot's shooting direction is changed by tilting it up and down with a stepper motor and turning it left and right with a DC motor that is controlled using PID control. The figure below explains how the robot works. \
<img src = "/doc/concept.JPG" height = "600"> \
The figure below shows the parts used to build the robot. \
<img src = "/doc/parts.JPG" height = "600"> \
A user interface was also built to control the robot, but this interface's code is on one of the other group members' computers. The biggest lesson from this project was that air pressure is very unreliable.
